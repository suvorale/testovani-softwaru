package cz.fel.cvut.ts1;

import cz.cvut.fel.ts1.Suvorale;
import org.junit.jupiter.api.Test;
import static org.junit.jupiter.api.Assertions.*;

public class SuvorovaTest {

    @Test
    public void factorialTest() {
        Suvorale s = new Suvorale();
        assertEquals(1, s.factorial(0));
        assertEquals(1, s.factorial(1));
        assertEquals(6, s.factorial(3));
    }
}
