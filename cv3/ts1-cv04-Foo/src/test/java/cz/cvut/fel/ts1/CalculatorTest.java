package cz.cvut.fel.ts1;


import org.junit.jupiter.api.*;

import static org.junit.jupiter.api.Assertions.*;

@TestMethodOrder(MethodOrderer.OrderAnnotation.class)
public class CalculatorTest {

    static Calculator c;

    @BeforeAll
    public static void initCalculator(){
        //Arrange
        c = new Calculator();
    }

    @Test
    @Order(1)
    public void add_addingInts_returnsRightValue(){
        int result = c.add(2, 3);
        assertEquals(5, result);
    }

    @Test
    @Order(2)
    public void subtract_addingInts_returnsRightValue(){
        int result = c.add(7, 2);
        assertEquals(5, result);
    }

    @Test
    @Order(3)
    public void multiply_addingInts_returnsRightValue(){
        int result = c.add(3, 2);
        assertEquals(6, result);
    }

    @Test
    @Order(4)
    public void divide_addingInts_returnsRightValue(){
        int result = c.add(6, 2);
        assertEquals(3, result);
    }

    @Test
    @Order(5)
    public void divide_divideByZero_throwsException(){
        Exception e = assertThrows(IllegalArgumentException.class, () -> c.divide(3, 0));

        String expectedMessage = "Can't divide by 0";
        String actualMessage = e.getMessage();

        // ASSERT 2
        Assertions.assertEquals(expectedMessage, actualMessage);
    }

}
